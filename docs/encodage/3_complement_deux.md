---
author: Mireille Coilhac
title: Entiers relatifs
---

???+ note dépliée

    &#129300; Le but est de trouver un moyen, avec des 0 et des 1 de représenter un entier négatif.

    Comment coder – 6 ?

## I. Une première piste

On pourrait imaginer utiliser un bit de signe.

👉 On convient de travailler sur 8 bits, ce qui est plus simple, et ne change rien à la compréhension du principe.

6 est codé sur 8 bits en binaire par : 

???+ question

    6 est codé sur 8 bits en binaire par : 

    ??? success "Solution"
        0000 0110

???+ question

    &#129300; On peut imaginer de réserver le bit de gauche (de poids fort) au signe, et de lui donner la valeur 0 pour un signe positif et 1 pour un signe négatif.

    &#128073; Avec cette convention, - 6 serait codé sur 8 bits en binaire par :

    ??? success "Solution"
        1000 0110


???+ question

    Avec cette convention, réaliser en binaire l’opération 6 + (-6) :

    ??? success "Solution"
    
        ```
                 11
            0000 0110
          + 1000 0110
          ____________
          = 1000 1100
        ```

???+ question "&#127797; Quel est le problème ?"

    ??? success "Solution"
        😭 Le résultat de 6 +(- 6) ne donnerait pas 0, mais - 12


## II. Recherche de l’opposé d’un entier

???+ note dépliée

    &#128161; On va chercher à trouver une représentation de - 6 sur 8 bits, qui respecte le fait que 6 + (- 6 ) = 0

???+ question "Pour cela, compléter l'addition "à trou" suivante"

    ```
      0000 0110
    +
    ___________
    = 0000 0000
    ```

    ??? success "Solution"

        ```
          1 1111 11
            0000 0110
          + 1111 1010
         ____________
          = 0000 0000
        ``` 
        

??? note dépliée "On trouve donc qu'une représentation de - 6 pourrait être :"

    1111 1010

!!! info "Le bon choix"

    💡 C'est effectivement cette représentation qui va être choisie. La méthode est ingénieuse : le 1 le plus à gauche (le bit de poids fort) du résultat "disparaît" car on ne dispose que de 8 bits. Le résultat devrait être 1 0000 0000, mais seuls les 8 bits contenant 0000 0000 sont conservés.


!!! info "Précision"
  
    L'opposé d'un entier, est une sorte de "complément à 0". Ici, en fait, nous avons  fait un "complément à 1 0000 0000", 
    c'est à dire en décimal à $2^8$. Dans la pratique, on dit simplement qu'on a fait le complément à 2.

!!! info "Généralisation sur $n$ bits"

    La méthode se transpose sur $n$ bits. On fait donc un complément à $2^n$, qui est toujours appelé **complément à 2**.


## III. Méthode du complément à 2

!!! info "Comment faire ? "
    
    Nous allons voir sur notre exemple, une méthode qui donne le même résultat, sans poser l'addition à trou.

    * Etape 1 : coder 6 **sur 8 bits**, et donc bien mettre tous les 0 nécessaires à gauche : 0000 0110
    * Etape 2 : "inverser les bits" : 1111 1001
    * Etape 3 : Ajouter 1

!!! example "Exemple"

    ```
             1
      1111 1001
    + 0000 0001
    ___________
    = 1111 1010
    ```

    😊 On retrouve le résultat que l'on avait avec l'addition "à trou"

!!! abstract "Résumé : Pour écrire un entier négatif en complément à 2 sur $n$ bits :"

    * Coder la partie positive de l'entier (sa valeur absolue) **sur $n$ bits** en binaire, **en rajoutant des 0 à gauche si nécessaire**.
    * Inverser tous les bits.
    * Ajouter 1.

!!! info "Vocabulaire : le complément à 1 "

    &#128073; Inverser tous les bits s'appelle faire le complément à 1.
    Par exemple le complément à 1 de 1010 1010 est 0101 0101.
    L'étape 2 consiste donc à faire le complément à 1.


!!! info "Notations"

    Il existe plusieurs notations possibles :

    $(-6)_{10}=(11111010)_{c2}=(11111010)_{A2}=(11111010)_{cpt2}$ etc.

    Le contexte permet de comprendre qu'il s'agit d'un entier codé en complément à 2.

???+ question "A vous de jouer 1 !"

    Utiliser la méthode du complément à 2 pour coder sur 8 bits : - 36

    ??? success "Solution"

        * 36 sur 8 bits : 0010 0100
        * Inversion des bits : 1101 1011
        * Ajout de 1 : 1101 1100

        👉 La réponse est donc : 1101 1100

???+ question "A vous de jouer 2 !"

    Utiliser la méthode du complément à 2 pour coder sur 8 bits : - 75

    ??? success "Solution"

        * 75 sur 8 bits : 0100 1011
        * Inversion des bits : 1011 0100
        * Ajout de 1 : 1011 0101
        
        👉 La réponse est 1011 0101


!!! info "Convertir en décimal un nombre donné en complément à 2"
    
    ???+ question  "Exemple"

        Le codage sur 8 bits de - 6 en complément à 2 est : 1111 1010

        Calculer : $- 2^7 + 2^6 + 2^5 + 2^4 + 2^3 + 2^1$

        Qu'obtenez-vous?

        ??? success "Solution"

            $- 6$

    ???+ question  "Faire un calcul analogue pour - 36"

        ??? success "Solution"

            Le codage sur 8 bits de - 36 en complément à 2 est : 1101 1100

            $- 2^7 + 2^6  + 2^4 + 2^3 + 2^2 = - 36$


???+ question  "Faire un calcul analogue pour - 75"

    ??? success "Solution"

        Le codage sur 8 bits de - 75 en complément à 2 est : 1011 0101

        $- 2^7 + 2^5  + 2^4 + 2^2+1 = - 75$

!!! abstract "Résumé :"

    Pour convertir un nombre négatif codé en complément à 2 en décimal, on procède comme s’il était codé en binaire, sauf que le terme correspondant au bit de poids fort est multiplié par -1.


???+ question  

    Par exemple le nombre 1010 1010 en complément à 2 est égal en base 10 à :

    ??? success "Solution"
        
        $-2^7 + 2^5+ 2^3+ 2^1 = -128+32+8+2= - 86$


!!! danger "&#9999;&#65039; Un petit problème : "

    ???+ question "Essayer de coder sur 8 bits, avec la méthode du complément à 2 : - 135."

        ??? success "Solution"

            * 135 sur 8 bits : 1000 0111
            * Inversion des bits : 0111 1000
            * Ajout de 1 : 0111 1001

            👉 La réponse est 0111 1001

        ???+ question "&#128546; Pourquoi ce résultat est-il impossible ?"

            ??? success "Solution"

                Le bit de poids fort est à 0, ce qui correspond à un entier positif.

## IV. Entiers que l’on peut représenter en complément à deux :

!!! info "Sur 8 bits"

    * Sur 8 bits, le plus petit entier que l’on peut représenter est donc :

    $-1 \times 2^7 + 0 \times 2^6+ 0 \times 2^5+ 0 \times 2^4+ 0 \times 2^3+ 0 \times 2^2+ 0 \times 2^1+ 0 \times 2^ 0= -128 = -2^7$	    
    
    Il est codé par	1000 0000.
    
    * Sur 8 bits, le plus grand entier que l’on peut représenter est donc :  

    $0 \times 2^7 + 1 \times 2^6+1 \times 2^5+ 1 \times 2^4+ 1 \times 2^3+ 1 \times 2^2+ 1 \times 2^1+ 1 \times 2^ 0= 127 = 2^7-1$   
    
    Il est codé par	0111 1111.


!!! abstract "Résumé :"

    Avec $n$ bits, il devient possible de représenter $2^n$ entiers compris entre $&#8722;2^{n&#8722;1}$ et $2^{n&#8722;1}&#8722;1$.

## V. Exercices

???+ question "Exercice 1"
  
    Trouvez les valeurs de 

    a) $(1111\ 0111)_{cpt2}$  
    b) $(1011\ 0101)_{cpt2}$

    ??? success "Solution"

        a) $- 2^7 + 2^6 + 2^5+ 2^4  + 2^2 + 2^1+  1=-9$

        b) $- 2^7 + 2^5+ 2^4+ 2^2 +  1=- 75$
    

## VI. QCM

???+ question "1. Que peut-on dire de l'entier relatif $10100001_2$ codé sur 8 bits en complément à deux ?"


    === "Cocher la ou les affirmations correctes"
        
        - [ ] a) Cet entier est pair.
        - [ ] b) Cet entier est négatif.
        - [ ] c) Cet entier est positif.
        - [ ] d) Cet entier vaut $-23_{10}$.

    === "Solution"
        
        - :x: 
        - :white_check_mark: 
        - :x:
        - :x: 

???+ question "2. Si 1011 est un entier signé sur quatre bits en complément à 2, alors sa valeur décimale est :"


    === "Cocher la ou les affirmations correctes"
        
        - [ ] a) -7
        - [ ] b) - 5
        - [ ] c) 5
        - [ ] d) 7

    === "Solution"
        
        - :x: 
        - :white_check_mark: 
        - :x:  
        - :x: 

???+ question "3. Donner l'écriture de $-3_{10}$ en complément à deux sur quatre bits :"


    === "Cocher la ou les affirmations correctes"
        
        - [ ] a) 0111
        - [ ] b) 1101
        - [ ] c) 1011
        - [ ] d) 1111

    === "Solution"
        
        - :x: 
        - :white_check_mark: 
        - :x:  
        - :x: 
