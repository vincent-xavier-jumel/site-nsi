---
author: Mireille Coilhac
title: Crédits
---

Le chapitre sur les tableaux a été réalisé par N. Revéret, Pierre Marquestaut, Jean-Louis Thirot et Mireille Coilhac avec l'aide des membre du groupe e-nsi .
L'ensemble des documents sont sous licence [CC-BY-NC-SA 4.0 (Attribution, Utilisation Non Commerciale, ShareAlike)](https://creativecommons.org/licenses/by-nc-sa/4.0/).

Le chapitre sur les dictionnaires a été réalisé par Mireille Coilhac avec la contribution de Charles Poulmaire.

Le chapitr HTML et CSS  a été réalisé par Fabrice Nativel, Jean-Louis Thirot, Valérie Mousseaux et Mireille Coilhac.

Le site est hébergé par la forge de [l'*Association des Enseignants d'Informatique de France*](https://aeif.fr/index.php/accueil/).

Le site est construit avec [`mkdocs`](https://www.mkdocs.org/) et en particulier [`mkdocs-material`](https://squidfunk.github.io/mkdocs-material/).

Un grand merci à M. Vincent-Xavier Jumel et Vincent Bouillot qui ont réalisé la partie technique de ce site.

Le logo :material-kayaking: fait partie de mkdocs sous la référence kayaking
