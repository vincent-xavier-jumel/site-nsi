---
author: Jean-Louis THIROT et Mireille Coilhac
title: Tuples - À vous
---  

!!! info "Les tuples"

	Les **tuples** (appelés **p-uplets** dans le programme officiel de NSI) sont une collection d'objets ordonnée mais ...

	on dit que ce sont des objets $\hspace{8em}$ ou $\hspace{10em}$, on dit aussi parfois $\hspace{10em}$ (tous ces termes sont synonymes).

!!! info "Syntaxe"

	Un tuple contient des valeurs, séparées par des virgules, mais encadrée par des parenthèses, ( ) ce qui les différencie des listes.

	!!! example "Exemple"

		```python
    	tuple_1 = (2, 1, 7, 0) # tuple_1 est un tuple de nombres entiers
    	tuple_2 = ("albert", "paul", "jacques") # tuple_2 est un tuple de `str`
    	tuple_3 = ("1G4","NSI", 22, 13.7, True) # tuple_3 contient des éléments de différents types. 
    	ma_liste = [2, 1, 7, 0] # ma_liste est une **liste** de nombres entiers
		```


!!! info "Les types des tuples"

	Les tuples sont de type ...

!!! info "Indices"

	Les éléments d'un tuples sont indicés de la même façon que ceux d'une liste (indice ... pour le premier élément).

!!! info "Les tuples sont immuables"

	!!! bug "Bug"

		Le code ci-dessous lèvera une exception TypeError 

		```python
		mon_tuple = ("a", "b", "c")
		mon_tuple[1] = "e"
		```

	On obtiendra en console, après exécution e ce code :

	```pycon
	TypeError: 'tuple' object does not support item assignment
	```

!!! info "Longueur"

	La fonction $\hspace{8em}$ renvoie la longueur d'un tuple (comme pour les chaînes et les listes) :


!!! info "Parcours 💚"

	Ces codes sont à connaitre **sans aucune hésitation** (ils sont strictement identiques aux parcours d'une liste).

	```python
	for i ... :
        print(mon_tuple[i])
    ```

    ```python
    for ... :
        print(element)
    ```

!!! info "Conversion entre listes et tuples"

	Il est possible de transformer une liste en tuple, et inversement :

	```python
	mon_tuple = (1, 2, 3)
	ma_liste = ...         # ma_liste contient les mêmes éléments : [1, 2, 3]
	```

	```python
	ma_liste = [1, 2, 3]
	mon_tuple = ...        # mon_tuple contient les mêmes éléments : (1, 2, 3)
	```

!!! info "Choisir la bonne structure"

	Il est préférable d'utiliser un tuple quand on veut regrouper des valeurs, et qu'on n'aura $\hspace{10em}$ de modifier les éléments ensuite. Les tuples étant $\hspace{8em}$ on ne risque pas, dans une partie du code, de modifier accidentellement les valeurs (ce qui, dans le cas d'une liste, peut très facilement arriver avec les effets de bords, que nous étudierons plus tard).

	!!! example "Exemple"

		* Je souhaite créer une liste de nombres, que je vais ensuite devoir trier dans l'ordre croissant : je choisi  ...
		* Je souhaite créer une liste de mots constituant un dictionnaire. Je serais amené plus tard à ajouter des mots dans ma liste :  je choisi ...
		* Dans un code, je porte sur une carte des villes. Pour chaque ville, j'ai un nom et les coordonnées géographiques. Pour chaque ville je crée un  ... 




