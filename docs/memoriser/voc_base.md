---
author: Mireille Coilhac
title: Vocabulaire de base - Bilan
---

## Affectation

!!! info "Affectation"

    Une affectation est une instruction de type : 

	`ma_variable = expression`

	L'expression **à  droite** du `=` est d'abord évaluée, puis le résultat est écrit en mémoire, et le nom de la variable permet de réutiliser cette valeur.



## Expression

!!! info "Expression"

	Une expression n'est pas à proprement parler une instruction, c'est quelque chose que python peut évaluer (c'est-à-dire donner sa valeur) : 

	Par exemple, dans : `var = expression`, `expression` peut-être : un nombre, un calcul, une chaine de caractères, un booléen ou des opérations plus complexes que nous verrons plus tard.

!!! info "Expression booléenne"

	Par exemple : `a == 3` est une expression booléenne.

	Donc l'instruction : `b = a == 3` est l’affectation  à la variable `b` du résultat de l'expression boolénne `a == 3`.


## Dérouler un code

!!! warning "Lire et comprendre "

    Pour lire et comprendre ce que fait un code, il faut souvent écrire **sur une feuille** le déroulé de ce code.

!!! example "Exemple"

    Considérons ce code : 

    ```python linenums='1'
    a = 1
	b = 3
	c = a + b
	```

	On peut décrire le déroulement du code en faisant un tableau.

	|n° ligne|a|b|c|
	|:--|:--|:--|:--|
	|1|1| | | 
	|2|1| 3| |
	|2|1| 3| 4|








