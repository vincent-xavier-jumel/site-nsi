---
author: Mireille Coilhac
title: Algorithme d'échange - Bilan
---

!!! info "Echanger le contenu de deux variables"

	Nous désirons échanger le contenu des variables `a` et `b`

## Avec une variable temporaire 

```python
a = valeur_de_a
b = valeur_de_b
# problème : echanger les deux valeurs
temp = valeur_de_a
a = valeur_de_b
b = temp
```

## En utilisant des tuples

!!! info "💡 A connaître"

	👉  Il est aisé, en python, de faire cet échange de façon très simple
	

	```python
	a, b = b, a
	```

