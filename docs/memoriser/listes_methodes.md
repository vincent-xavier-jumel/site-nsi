---
author: Mireille Coilhac
title: Méthodes de listes Python - Bilan
---

!!! warning "Remarque"

    Il y a toujours des parenthèses, éventuellement vides, pour les méthodes.
    Les méthodes utilisent la notation pointée.


## I. Les indispensables

!!! abstract "`list.append(x)`"

    Ajoute un élément à la fin de la liste. 


!!! abstract "`list.remove(x)`"

    Supprime de la liste le **premier** élément dont la **valeur** est égale à `x`. 


!!! abstract "`list.pop(i)`"

    * Enlève de la liste l'élément situé à l'indice `i` et renvoie cet élément.
    * Si aucun indice n'est spécifiée, `a.pop()` enlève et renvoie le dernier élément de la liste.


!!! abstract "`list.sort()`"

    Ordonne les éléments dans la liste. La liste est modifiée (triée).  

## II. L'instruction `del`

!!! abstract "`del ma_liste[i]`"

    Enlève de la liste l'élément situé à l'indice `i` **mais ne renvoie pas** cet élément



## III. non indispensable

!!! abstract "`list.count(x)`"

    Renvoie le nombre d'éléments ayant la valeur `x` dans la liste.

