def accede_valeur(tableau, ligne, colonne) -> int :
    """
    Entrée : 
      - tableau est une liste de listes d'entiers
      - ligne et colonne sont deux int, les indices de ligne et de colonne
    Sortie : la fonction renvoie l'élément situé à la ligne d'indice ligne, colonne d'indice colonne

    Exemple :
    >>> m = [ [1, 3, 4], [5 ,6 ,8], [2, 1, 3] ]
    >>> accede_valeur(m, 2, 0)
    2
    """
    return tableau[ligne][colonne]

    