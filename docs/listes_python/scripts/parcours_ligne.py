def get_ligne(lst, i) -> list :
    """
    Entrées : 
      - lst : une liste de listes
      - i :un entier
    Sortie : une liste dont les éléments sont les éléments de la ligne i
    """
    ...


# Tests
m = [[1, 3, 4],
     [5, 6, 8],
     [2, 1, 3],
     [7, 8, 15]]
assert get_ligne(m, 0) == [1, 3, 4]
assert get_ligne(m, 3) == [7, 8, 15]