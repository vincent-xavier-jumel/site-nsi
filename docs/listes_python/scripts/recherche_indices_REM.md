La solution proposée ci-dessus est la version recommandée.

Version (moins élégante) sans compréhension de liste :

```python
def indices(element, entiers):
    positions = []
    for i in range(len(entiers)):
        if entiers[i] == element:
            positions.append(i)
    return positions
```