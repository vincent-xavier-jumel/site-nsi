---
author: Gilles Lassus et Mireille Coilhac
title: Algorithmes de recherche naïve et dichotomique
---

## I. Introduction

🔎 La recherche d'un élément dans une liste est un problème fréquemment rencontré dans les algorithmes.  
La fonction <font color="blue">``in``</font> de python fait cela :

???+ question "Tester"

    {{ IDE('scripts/fct_in') }}


## II. Recherche naïve dans une liste

!!! info "Balayage"

     La façon la plus simple de chercher une valeur dans une liste est de parcourir un à un les éléments de la liste (de manière séquentielle). On compare les éléments à la valeur recherchée.

???+ question "Parcours séquentiel version longue"

    Compléter la fonction `appartient_1` dont les spécifications sont données.

    !!! danger "Contrainte"

        Il est interdit d'écrire `if element in tableau:`   


    {{ IDE('scripts/appartient_1') }}  

???+ question "Parcours séquentiel version courte"   

    🌵 Le problème de cette technique, c'est qu'on peut continuer à parcourir le tableau jusqu'au bout, pour rien, si on a trouvé l'élément avant la fin.  

    👉 Il est recommandé de faire une sortie anticipée de la boucle en utilisant le `return`

    Compléter la fonction `appartient_2` dont les spécifications sont données en utilisant une sortie anticipée.

    !!! danger "Contrainte"

        Il est interdit d'écrire `if element in tableau:`   


    {{ IDE('scripts/appartient_2') }} 


## III. La dichotomie

### 1. Une première approche

???+ question "Je joue contre l'ordinateur"

    Question 1.   

    Exécuter le programme du jeu du nombre mystère.
    Faire quelques parties, expliquer la stratégie de l'ordinateur pour trouver le nombre mystère.

    {{ IDE('scripts/mystere') }} 

??? success "Solution"

        A chaque fois, il se place au milieu.

???+ question "Je joue contre l'ordinateur : suite"

    Question 2.

    Lorsque le nombre est compris entre 1 et 100, en combien d'essais au maximum l'ordinateur trouve-t-il la solution ?

    ??? success "Solution"

        En 7 essais.

    Question 3.

    Et si le nombre mystère est compris entre 1 et 200 ?

    ??? success "Solution"

        En 8 essais.



### 2. La dichotomie

!!! info "La dichotomie"

    Le mot dichotomie vient du grec ancien διχοτομία, dikhotomia (« division en deux parties »).

    La méthode de dichotomie fait partie des méthodes dites *«diviser pour régner»*. 

    «dichotomie» se dit en anglais *binary search*.


### 3. Algorithme de recherche dichotomique

!!! note "Dichotomie, déroulement intuitif"
    - on se place *au milieu* de la liste.
    - on regarde si la valeur sur laquelle on est placée est inférieure ou supérieure à la valeur cherchée.
    - on ne considère maintenant que la bonne moitié de la liste qui nous intéresse.
    - on continue jusqu'à trouver la valeur cherchée (ou pas)



Comprendre la méthode de dichotomie est relativement simple, mais savoir la programmer est plus difficile.

Pour des raisons d'efficacité, nous allons garder *intacte* notre liste de travail et simplement faire évoluer les indices qui déterminent le début et la fin de notre liste.

Une autre méthode pourrait être d'extraire à chaque étape une nouvelle liste (dont on espère qu'elle contient la valeur cherchée), mais la technique utilisée (le *slicing* de liste) consomme beaucoup trop de ressources.

Nous allons donc travailler avec trois variables :

- `indice_debut` (en bleu sur le schéma)
- `indice_fin` (en bleu sur le schéma)
- `indice_central`, qui est égale à `(indice_debut + indice_fin) // 2` (en rouge sur le schéma)

Dans cet exemple nous cherchons `14` dans la liste **triée** `[2, 3, 6, 7, 11, 14, 18, 19, 24]`.

![indices dichotomie](images/fig4.png){: .center}

Nous allons faire *se rapprocher* les indices `indice_debut` et `indice_fin` **tant que** `indice_debut <= indice_fin`

### 4. Recherche d'appartenance

???+ question "Appartenance"

    Compléter la fonction `appartient_dichotomique` qui prend en paramètre une liste Python `ma_liste` et une valeur `valeur`. Cette fonction renvoie `True` si `valeur` est dans `ma_liste` et `False` sinon.

    {{ IDE('scripts/appartient_dicho') }}


???+ question "Déroulé à la main : `v = 9` est-il dans `t = [1, 3, 6, 9]` ?"

    Ecrire le déroulé à la main. Voici le début à poursuivre de façon analogue : 

    * indice_debut = <font color="#cc7777"><b>0</b></font>
    * indice_fin = <font color="#cc7777"><b>3</b></font>
    * condition du while : <font color="#cc7777"><b>True</b></font>
    * indice_centre =  <font color="#cc7777">**(0+3)//2 = 1**</font>
    * valeur_centrale = ma_liste[indice_centre]  = <font color="#cc7777"><b>3</b></font>
    * v == valeur_centrale → `False`
    * valeur_centrale < v → `True`
    * indice_debut =  <font color="#cc7777">**2**</font>
    * condition du while : <font color="#cc7777"><b>True</b></font>
    * ...


    ??? success "Solution"

        * indice_debut = <font color="#cc7777"><b>0</b></font>
        * indice_fin = <font color="#cc7777"><b>3</b></font>
        * condition du while : <font color="#cc7777"><b>True</b></font>
        * indice_centre =  <font color="#cc7777">**(0+3)//2 = 1**</font>
        * valeur_centrale = ma_liste[indice_centre]  = <font color="#cc7777"><b>3</b></font>
        * v == valeur_centrale → `False`
        * valeur_centrale < v → `True`
        * indice_debut =  <font color="#cc7777">**2**</font>
        * condition du while : <font color="#cc7777"><b>True</b></font>
        * indice_centre =  <font color="#cc7777">**(2+3)//2 = 2**</font>
        * valeur_centrale = ma_liste[indice_centre]  = <font color="#cc7777"><b>6</b></font>
        * v == valeur_centrale → `False`
        * valeur_centrale < v → `True`
        * valeur_debut =  <font color="#cc7777">**3**</font>
        * condition du while : <font color="#cc7777"><b>True</b></font>
        * indice_centre =  <font color="#cc7777">**(3+3)//2 = 3**</font>
        * valeur_centrale = ma_liste[indice_centre]  = <font color="#cc7777"><b>9</b></font>
        * v == valeur_centrale → `True`
        * la fonction renvoie `True`



???+ question "Déroulé à la main : `v = 7` est-il dans `t = [1, 3, 6, 9, 10]` ?"

    Ecrire le déroulé à la main, comme dans l'exercice précédent

    ??? success "Solution"

          * indice_debut = <font color="#cc7777"><b>0</b></font>
          * indice_fin = <font color="#cc7777"><b>4</b></font>
          * condition du while : <font color="#cc7777"><b>True</b></font>
          * indice_centre =  <font color="#cc7777">**(0+4)//2 = 2**</font>
          * valeur_centrale = ma_liste[indice_centre]  = <font color="#cc7777"><b>6</b></font>
          * v == valeur_centrale → `False`
          * valeur_centrale < v → `True`
          * indice_debut =  <font color="#cc7777">**3**</font>
          * condition du while : <font color="#cc7777"><b>True</b></font>
          * indice_centre =  <font color="#cc7777">**(3+4)//2 = 3**</font>
          * valeur_centrale = ma_liste[indice_centre]  = <font color="#cc7777"><b>9</b></font>
          * v == valeur_centrale → `False`
          * valeur_centrale < v → `False`
          * indice_fin =  <font color="#cc7777">**2**</font>
          * condition du while : <font color="#cc7777"><b>False</b></font>
          * la fonction renvoie `False`

          👉 On voit dans cet exemple pourquoi l'instruction `while indice_debut <= indice_fin :` est **absolument nécessaire**.


### 5. Recherche d'indice

???+ question "Recherche"

    Compléter la fonction `recherche_dichotomique` qui prend en paramètre une liste Python `ma_liste` et un valeur `valeur`. Cette fonction renvoie son indice si `valeur` est dans `ma_liste` et `None` sinon.

    {{ IDE('scripts/recherche_dicho') }}


## IV. Exercice

???+ question "Une fête"

    Nicolas organise une fête, et demande à ses amis s'ils viendront. Dès qu'un ami lui répond favorablement, il l'ajoute dans `liste_amis`.
    Compléter le code ci-dessous afin de pouvoir déterminer si Vincent, Romain et Valérie ont décidé de venir (bien respecter les majuscules, minuscules et accents). La liste `liste_amis` est dans du code caché.

    Vous devez **absolument** réaliser une recherche **dichotomique** et pas une recherche naïve. Attention, c'est à vous de créer `ma_liste_amis` qui est utilisée dans les tests. (Vous pouvez regader l'astuce plus bas, en cas de besoin)
    {{IDE('scripts/exo_dicho')}}

??? tip "Astuce"

    N'y-a-t-il pas une condition sur la liste dans laquelle on réalise la recherche dichotomique ?
        

??? success "Solution pour Vincent, Romain et Valérie"

    ```pycon
    >>> appartient_dichotomique(ma_liste_amis, "Vincent")
    False
    >>> appartient_dichotomique(ma_liste_amis, "Romain")
    False
    >>> appartient_dichotomique(ma_liste_amis, "Valérie")
    True
    >>> 
    ```

## V. Bilan

??? note "A savoir par 💚 (cliquer)"

    ```python
    def recherche_dichotomique(ma_liste, valeur) :
        indice_debut = 0
        indice_fin = len(ma_liste) - 1
        while indice_debut <= indice_fin :  # (1)
            indice_centre = (indice_debut + indice_fin) // 2  # (2)
            valeur_centrale = ma_liste[indice_centre]
            if valeur_centrale == valeur :
                return indice_centre
            if valeur_centrale < valeur :
                indice_debut = indice_centre + 1  # (3)
            else :
                indice_fin = indice_centre - 1  # (4)
        return None
    ```

    1. :warning: Il faut bien `<=` et pas `<` 
    2. :warning: Il faut une division **entière** donc `//` et pas `/`
    3. 👉 On cherche à droite
    4. 👈 On cherche à gauche

    !!! attention "Prenez le temps de lire les commentaires (cliquez sur les +)"


## VI. Crédits

Auteurs : Gilles Lassus, Fabrice nativel, Jean-Louis Thirot, Valérie Mousseaux et Mireille Coilhac







