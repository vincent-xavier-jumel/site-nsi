---
author: Mireille Coilhac
title: Spécifications
---

!!! warning "Installations avant de commencer"


    **1.** Télécharger les deux fichiers ci-dessous, et les enregistrer dans un même dossier.

    🌐 TD à télécharger : Fichier `specifs.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/specifs.ipynb)

    🌐 Module à télécharger : Fichier `module_Alice.py` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/module_Alice.py)

    **2.** suivre ce lien, puis ouvrir le fichier `specifs.ipynb` : [Basthon](https://notebook.basthon.fr/){ .md-button target="_blank" rel="noopener" }

    **3.** Vous devez charger ce module : `module_Alice.py` dans le notebook ouvert sur Basthon :  
    icone "ouvrir fichier" puis choisir "Installer le module"


???+ questions "A faire vous-même 1 et à faire vous-même 2 du TD"

    Vous pourrez vous référer au cours sur les modules

    ??? success "Solution A faire vous-même 1"

        `dir(module_Alice)`

    ??? success "Solution A faire vous-même 2"

        ```python
        help(module_Alice.fonction_1)
        help(module_Alice.fonction_2)
        ```
   

???+ question "Bilan du TD"

    Après avoir réalisé le TD précédant répondre aux questions :

    **1.** Quelle est la syntaxe, et à quoi sert la spécification d'une fonction ?

    ??? success "Solution"

    	* La spécification s'écrit au début de la fonction entre `"""` et `"""`
    	* On parle aussi souvent de **"docstring"**
    	* Elle sert à préciser les paramètres, ce que renvoie la fonction, et son rôle

    **2.** Quelle est la syntaxe, et à quoi sert la fonction `help` ?

    ??? success "Solution"

    	`help(ma_fonction)` renvoie la docstring de la fonction `ma_fonction`



