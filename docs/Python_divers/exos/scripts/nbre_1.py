def compte_uns(tableau):
    ...


# Tests
assert compte_uns([0, 1, 1, 1]) == 3
assert compte_uns([0, 0, 0, 1, 1]) == 2
assert compte_uns([0] * 200) == 0
assert compte_uns([1] * 300) == 300
assert compte_uns([0] * 200 + [1] * 500) == 500
assert compte_uns([]) == 0
