Le premier test permet de traiter le cas des tableaux vides. Le second celui des tableaux ne contenant que des`#!py 1`. On renvoie alors directement la longueur du tableau. Ce cas pourrait être traité par la boucle principale mais ce test initial permet d'optimiser le code.

Le tableau peut être partagé en deux zones (éventuellement vides) : une zone ne contenant que des `#!py 0`, une autre ne contenant que des `#!py 1`.

On utilise une recherche dichotomique afin de trouver la position du premier `#!py 1`, limite entre ces deux zones. Il s'agit de l'unique `#!py 1` précédé par un `#!py 0`. Une fois trouvé on renvoie le nombre de `#!py 1`.

Si l'on quitte la boucle sans avoir renvoyé de résultat c'est que le tableau ne contenait aucun `#!py 1`. On peut renvoyer `#!py 0`.
