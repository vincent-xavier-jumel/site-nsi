def histogramme(caracteres):
    histo = {}
    for caractere in caracteres:
        if caractere in histo:
            histo[caractere] += 1
        else:
            histo[caractere] = 1
    return histo

h = histogramme("brontosaurus")
print(h)
