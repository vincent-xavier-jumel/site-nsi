---
author: Jean-Louis Thirot, Valérie Mousseaux et Mireille Coilhac
title: Compléments
---

## I. Chemins absolus ou relaifs

Nous allons étudier l'exemple suivant : 

![Arborescence](images/chemins_fond.png){ width=70% .center}

!!! info "Chemin absolu ou relatif"

	Pour indiquer la position d'un fichier (ou d'un dossier) dans l'arborescence, il existe 2 méthodes :

	* indiquer un chemin **absolu**. Le chemin absolu doit indiquer « le chemin » depuis la racine.
	Par exemple l'URL du fichier `fichier3.jpg` sera : `/dossier2/dossier3/fichier3.jpg`
	Remarquez que nous démarrons bien de la racine **`/`** (attention les symboles de séparation sont aussi des `/`)

	* indiquer un chemin **relatif**.Imaginons maintenant que le fichier fichier1.css fasse appel au fichier `fichier3.jpg` (comme un fichier HTML peut faire appel à un fichier CSS). Il est possible d'indiquer le chemin non pas depuis la racine, mais depuis le dossier `dossier2` qui accueille le `fichier1.css`, nous parlerons alors de chemin relatif :
	`dossier3/fichier3.jpg `

	👉 Remarquez l’absence du `/` au début du chemin (c'est cela qui nous permettra de distinguer un chemin relatif et un chemin absolu).

!!! example "Autre exemple de chemin relatif"

    Imaginons maintenant que nous désirions indiquer le chemin relatif du fichier `fichier5.svg` depuis l'intérieur du dossier `dossier4`.

	Comment faire ?

	Il faut "remonter" d'un "niveau" dans l'arborescence pour se retrouver dans le dossier dossier2 et ainsi pouvoir repartir vers le `dossier3`. Pour ce faire il faut utiliser 2 points : **`..`**

	`../dossier3/fichier5.svg`


!!! example "Autre exemple de chemin relatif"

	😊 Il est tout à fait possible de remonter de plusieurs "crans" : `../../` depuis le dossier  `dossier4` permet de revenir à la racine.

???+ question "1. Chemin relatif"

    Depuis le `dossier3`, donner le chemin relatif qui permet d'obtenir `fichier4.js`

    ??? success "Solution"

    	`../../dossier1/fichier4.js`

???+ question "2. Chemin relatif"

    Donnez le chemin relatif permettant d'atteindre le fichier `fichier5.svg` depuis le dossier `dossier4`

    ??? success "Solution"

    	`../dossier3/fichier5.svg`

???+ question "3. Chemin absolu"

    Donnez le chemin absolu permettant d'atteindre le fichier `fichier6.html`

    ??? success "Solution"

    	`/dossier2/dossier4/fichier6.html`


## II. Le WEB sémantique


Vous l'avez compris, tout ce qui est affiché dans une page est une série de <b>"blocs"</b>, auxquels s'ajoutent des éléments
non affichés. La norme <b>HTML 5</b> est très stricte sur l'utilisation des balises <b>sémantiques</b> dont nous avons
déjà vu certaines.


Le HTML sémantique est l'utilisation du balisage HTML visant à renforcer le sémantisme (la signification) des 
informations contenues dans les pages web, c'est-à-dire leur sens, plutôt que de se borner à définir leurs présentations 
(ou apparence). Le HTML sémantique est traité par les navigateurs courants, mais aussi par de nombreux autres agents 
utilisateurs. Le langage CSS est utilisé pour suggérer la forme sous laquelle il sera présenté aux utilisateurs humains.


Par exemple, les spécifications HTML récentes déconseillent l'utilisation de la balise &lt;i>, qui indique un style de police 
italique, au profit de balises sémantiquement plus précises, telles que &lt;em>, qui indique une mise en valeur. C'est la 
feuille de style CSS qui spécifie ensuite si cette mise en valeur est représentée par un style italique, un style gras, un
 soulignement, une prononciation plus lente ou plus forte, etc. L'utilisation de l'italique ne se limite en effet pas à la mise 
en valeur. Il est également utilisé pour citer des sources, et pour cela, le HTML 5 propose la balise &lt;cite>. 


### 1. Balises structurelles

<ul>
<li><b class="blue">header</b> Regroupe les éléments de l'en-tête (du body, d'une section, d'un article...)</li>
<li><b class="blue">main</b>: Utilisée pour regrouper l'ensemble des sections ou articles, partie la plus importante du contenu.</li>
<li><b class="blue">nav</b> Utilisée pour regrouper les éléments du menu de navigation.</li>
<li><b class="blue">section</b> Regroupe les éléments d'un partie d'un document.</li>
<li><b class="blue">article</b> Comme une section mais le sens est légèrement différent</li>
<li><b class="blue">footer</b> Regroupe des éléments comme pied de page, mais peut aussi figurer en fin d'une section
par exemple avec un bouton "haut de page"</li>
<li><b class="blue">aside</b> Regroupe des éléments  tels que des notes, remarques, commentaires, compléments, non essentiels
pour la lecture du contenu, mais qui le complète.</li>
</ul>

### 2. Balises d'emphase 

<ul>
<li><b class="blue">h1 h2 h3... h7</b> Les balises de titres hiérarchisent les contenus</li>
<li><b class="blue">strong</b> Met en valeur un mot ou un groupe de mots. Les navigateurs font généralement
appraitre le texte entre <b class="blue">&lt;strong></b> et <b class="blue">&lt;/strong></b> en gras. La balise
<b class="blue">&lt;b>...&lt;/b></b> a aussi cet effet, mais celle ci n'a pas de sens particulier, alors que <b class="blue">&lt;strong></b> 
indique un mot ou groupe de mots importants (mots clefs, définitions etc...) </li>
<li><b class="blue">em</b>: Comme <b class="blue">&lt;strong></b> mais moins "fort".</li>
<li><b class="blue">mark</b>: Comme <b class="blue">&lt;strong></b> et <b class="blue">em</b> mais encore moins "fort" </li>
<li><b class="blue">cite</b> Début d'une citation.</li>
<li><b class="blue">blockquote</b> Regroupe les éléments tiré d'une autre source (qui sera généralement mentionnée).</li>
<li><b class="blue">code</b> Début d'une portion de contenu contenant du code (html, python, javascritpt etc...)</li>
</ul>


Il en existe de nombreuses autres (<b class="blue">adress</b>, <b class="blue">abbr</b> pour une abréviation, <b class="blue">q</b>
pour une citation rapide et courte etc...)


### 3. balises non sémantiques

Il est parfois nécessaire de regrouper des éléments (surtout pour des motifs de placement ou de formatage dans la page) sans que le groupe
constitue un ensemble auquel on puisse donner un sens. Pour cet usage on a conservé, en <b>HTML 5</b> une balise
qui existe depuis le début du <b>WEB</b> : La balise <b class="blue">div</b>.
 

son usage est extrêmement courant, bien que dans de nombreux cas, il soit incorrect, car une autre balise, sémantique, aurait put
être utilisée. Elle n'en demeure pas moins indispensable à connaitre pour des mises en page sophistiquées.

Exemple d'utilisation de <b class="blue">&lt;div></b></p>
 
Dans le code ci-dessous, un bloc <b class="blue">&lt;div></b> est utilisé pour regrouper 1 titre, un paragraphe et un 
bloc <b lass="blue">aside
</b>, le tout formant un bloc pour afficher une définition. A noter qu'au lieu de <b class="blue">&lt;div></b> il serait préférable 
ici d'utiliser <b class="blue">&lt;dfn></b>.
 
<pre class="left" style="background-color:#333333;color:white">&lt;section>
  &lt;h2>Titre de la section&lt;h2>  
  &lt;p>Un premier paragraphe&lt;p>
  &lt;div>
    &lt;h3>Définition&lt;/h3>
    &lt;p>
      Le texte de la définition
    &lt;/p>
    &lt;aside>
      Une remarque
    &lt;/aside>
  &lt;/div>
  &lt;h3>La suite de la section&lt;/h3>
&lt;/section>
</pre>



Le plus souvent, ce bloc <b class="blue">&lt;div> </b>est utilisé pour attribuer une classe aux éléments, nous verrons cela plus
loin. La classe permet de formater l'apparence des éléments concernés. Par exemple, on souhaite que cette section
apparaisse avec un fond de page d'une couleur différente du reste de la page....

Dans le même ordre d'idée, la balise <b class="blue">&lt;span>....&lt;/span></b> regroupe aussi des éléments (mais 
dans le cas de <b class="blue">&lt;span></b>, les éléments seront placés côte à côte, alors que <b class="blue">&lt;div></b> produira un placement 
des éléments les uns en dessous des autres.

