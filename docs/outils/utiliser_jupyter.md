---
author: Mireille Coilhac
title: Notebook Jupyter
--- 

## I. Jupyter sur votre ordinateur

!!! info "Avec EduPython"

	Vous pouvez utiliser un notebook Jupyter (fichier avec l'extension .ipynb) sur votre ordinateur sur Edupython, en ouvrant le menu Outils>Jupyter>  ou en cliquant directement ici : 

![Ouvrir sur EduPython](images/ouvrir_jupy_edupython.png){ width=90% }

![clic dossier](images/clic_dossier.png){ width=90% }

## II. Autre méthtode : Jupyter en ligne sur basthon.fr

👉 Sur le site [basthon](https://basthon.fr/){ .md-button target="_blank" rel="noopener" }

👉 Dans le menu, cliquez Notebook et choisir Python, ou directement cliquer sur l'icône Notebook

![nb basthon](images/basthon_nb.png){ width=90% }

👉 Sélectionner Fichier puis ouvrir l’icône « ouvrir », puis le fichier que vous voulez ouvrir. 