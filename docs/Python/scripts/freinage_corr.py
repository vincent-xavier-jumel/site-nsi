def reaction(vitesse):
    """
    Cette fonction renvoie la distance de réaction pour une
    certaine valeur de vitesse
    Par exemple reaction(50) doit renvoyer 13.888888....
    """
    dist_reac = vitesse / 3.6
    return dist_reac

def freinage(vitesse):
    """
    Cette fonction renvoie la distance de freinage pour une
    certaine valeur de vitesse
    Par exemple freinage(50) doit renvoyer 12.5
    """
    dist_freinage = vitesse ** 2 / 200
    return dist_freinage

def arret(vitesse):
    """
    Cette fonction renvoie la distance totale d arret pour une
    certaine valeur de vitesse
    Par exemple arret(50) doit renvoyer 26.388888....
    """
    # On appelle la fonction reaction et on affecte la
    # valeur renvoyée à la variable distance_reac
    distance_reac = reaction(vitesse)

    # On appelle la fonction freinage et on affecte la
    # valeur renvoyée à la variable distance_freinage
    distance_freinage = freinage(vitesse)

    # on calcule la distance d'arret et on renvoie le résultat
    distance_arret = distance_reac + distance_freinage

    return distance_arret

vitesse = float(input("Quelle est votre vitesse en km/h : "))
distance_reaction = reaction(vitesse)
distance_freinage = freinage(vitesse)
distance_arret = arret(vitesse)

# Test
# Vos affichages ci-dessous
print("distance parcourue pendant la reaction : ", distance_reaction, " m")
print("distance parcourue pendant le freinage : ", distance_freinage, " m")
print("distance parcourue pendant l arret : ", distance_arret, " m")


