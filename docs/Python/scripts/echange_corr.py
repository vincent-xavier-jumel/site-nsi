x = int(input("saisir x : "))
y = int(input("saisir y : "))
print("Avant échange : ")
print("x = ", x)
print("y = ", y)

temp = y
y = x
x = temp

print("Après échange : ")
print("x = ", x)
print("y = ", y)

