# plusieurs affectation :
x = 1
y = 3
somme = x + y

# afficher les adresses mémoire :
print("x réfère à l'adresse :", id(x), "ou est stocké la valeur", x)
print("y réfère à l'adresse :", id(y), "ou est stocké la valeur", y)
print("somme réfère à l'adresse :", id(somme), "ou est stocké la valeur", somme)