---
author: Jean-Louis Thirot et Mireille Coilhac
title: Python - Instructions conditionnelles
---

##. I. La syntaxe par l'exemple


???+ question "Juste un if"

    !!! warning inline end "Important"

        Prenez le temps de lire les commentaires !
    
        Cliquez sur les +
    

	```python
	prix = 50
	if prix > 20:  # (1)
    	print("Ce prix dépasse 20 euros")
    ```

    1. :warning: Le caractère `#!py :` est **obligatoire** après `if`, `elif`, `else`, `for` et `while`. Il provoque l'indentation automatique du bloc d'instruction qui suit.

    
    Tester :
    
    {{IDE('scripts/juste_if')}}

???+ question "if et else"

    !!! warning inline end "Important"

        Prenez le temps de lire les commentaires !
    
        Cliquez sur les +
    

	```python
	note = float(input("Saisir votre note : "))
	if note >= 10:  # (1)
    	print("reçu")
	else:  # (2)
    	print("refusé")
    ```

    1. :warning: Le caractère `#!py :` est **obligatoire** après `if`, `elif`, `else`, `for` et `while`. Il provoque l'indentation automatique du bloc d'instruction qui suit.

    2. :warning: Le caractère `#!py :` est **obligatoire** après `if`, `elif`, `else`, `for` et `while`. Il provoque l'indentation automatique du bloc d'instruction qui suit.


    Tester :
    
    {{IDE('scripts/if_else')}}

???+ question "if, elif et else"

    !!! warning inline end "Important"

        Prenez le temps de lire les commentaires !
    
        Cliquez sur les +
    

	```python
	note = float(input("Saisir votre note : "))
	if note >= 16:  # (1)
    	print("TB")  # (2)
	elif note >= 14:  # (3)
    	print("B")  # (4)
	elif note >= 12:  # (5)
    	print("AB")  # (6)
	elif note >= 10:
    	print("reçu") # (7)
	else:  # (8)
    	print("refusé")  # (9)
    ```

    1. :warning: Le caractère `#!py :` est **obligatoire** après `if`, `elif`, `else`, `for` et `while`. Il provoque l'indentation automatique du bloc d'instruction qui suit.

    2. Si on est arrivé là, cela signifie que `note` ≤ 16

    3. `elif` signifie **sinon si**.

    4. Si on est arrivé là, cela signifie que `note` < 16 et `note` ≥ 14 :   
    14 ≤ `note` < 16

    5. :warning: Le caractère `#!py :` est **obligatoire** après `if`, `elif`, `else`, `for` et `while`. Il provoque l'indentation automatique du bloc d'instruction qui suit.

    6. Si on est arrivé là, cela signifie que `note` < 14 et `note` ≥ 12 :   
    12 ≤ `note` < 14

    7. Si on est arrivé là, cela signifie que `note` < 12 et `note` ≥ 10 :   
    10 ≤ `note` < 12

    8. :warning: Le caractère `#!py :` est **obligatoire** après `if`, `elif`, `else`, `for` et `while`. Il provoque l'indentation automatique du bloc d'instruction qui suit.

    9. Si on est arrivé là, cela signifie que `note` < 10 . **On n'écrit jamais de condition après `else`**


    Tester :
    
    {{IDE('scripts/if_elif_else')}}

???+ question "if ... et if"

    !!! warning inline end "Important"

        Prenez le temps de lire les commentaires !
    
        Cliquez sur les +
    

	```python
	note = float(input("Saisir votre note : "))
	if note == 20:  # (1)
    	print("Parfait !")
	if note != 0:  # (2)
    	print("Ce n'est pas nul !")
    # (3)
    ```

    1. :warning: Il faut deux symboles `==` pour tester l'égalité. Un seul symbole `=`réalise une affectation.

    2. :warning: Différent s'écrit `!=`

    3. :warning: Il n'est pas obligatoire d'avoir un `else`


    Tester :

    Vous pourrez tester les saisies de 0, puis de 20, puis de 10 par exemple. Que se passe-t-il ?
    
    {{IDE('scripts/if_if')}}

??? success "Solution"

	Si on saisit 0 ... il ne se passe rien ...

	Lisez le code, c'est normal ! 😊

    Si on saisit 20, "on rentre" dans les deux tests `if`, et on obtient les deux affichages.


## II. Cours/TD


???+ question "Cours/TD"

    <div class="centre" markdown="span">
    <iframe 
    src="https://notebook.basthon.fr/?from={{ page.canonical_url }}../a_telecharger/instrucions_conditionnelles_2022_sujet.ipynb"
    width="900" height="700" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>

    😊 Voici la correction : 

    <div class="centre" markdown="span">
    <iframe 
    src="https://notebook.basthon.fr/?from={{ page.canonical_url }}../a_telecharger/instructions_conditionnelles_2022_correction.ipynb"
    width="900" height="700" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>


<!--- La correction à télécharger plus tard A SORTIR DE L'ADMONITION

⏳ La correction viendra bientôt ... 
👉 Bien sortir ce commentaire de l'admonition en supprimant l'indentation
🌐 Fichier `instructions_conditionnelles_2022_correction.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/instructions_conditionnelles_2022_correction.ipynb)
-->

## III. Exercices

???+ question "Exercice"

	👉 Il faut d'abord télécharger les trois fichiers suivants et les mettre dans le même dossier :

	🌐 TD à télécharger : Fichier `exos_instructions_conditionnelles_sujet.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/exos_instructions_conditionnelles_sujet.ipynb)

	🌐 module à télécharger : Fichier `_module_ex_conditionelles.py` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/_module_ex_conditionelles.py)

	🌐 image à télécharger : Fichier `plateau.png` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/plateau.png)

	👉 Il faut ensuite ouvrir le premier fichier exos_instructions_conditionnelles_sujet.ipynb en ligne avec [Basthon](https://notebook.basthon.fr/){ .md-button target="_blank" rel="noopener" }

    😊 La correction est arrivée : 

    🌐 Fichier `exos_instructions_conditionnelles_correction.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/exos_instructions_conditionnelles_correction.ipynb)
    

<!--- La correction à télécharger plus tard A SORTIR DE L'ADMONITION
⏳ La correction viendra bientôt ... 
👉 Bien sortir ce commentaire de l'admonition en supprimant l'indentation
🌐 Fichier `exos_instructions_conditionnelles_correction.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/exos_instructions_conditionnelles_correction.ipynb)
-->

## IV. Bilan

!!! danger "Attention à ne pas oublier les deux points et l'indentation."


!!! abstract "La syntaxe de if :"

    👉 Attention, l'instructions `else` n'est pas du tout obligatoire (Voir les exemples ci-dessus).  
	
	```
    # Avec seulement if
	if condition :
   		bloc d'instructions à réaliser
   	...
   	```

   	```
    # Avec if et else
	if condition :
   		bloc d'instructions à réaliser
   	else:
   		autre bloc d'instructions à réaliser
   	...
   	```


!!! abstract "La syntaxe de if ... elif ... else :"

    👉 Attention, l'instructions `else` n'est pas du tout obligatoire (Voir les exemples ci-dessus).  
	
	```
    # Avec seulement if et elif
	if condition :
   		bloc d'instructions à réaliser
   	elif autre condition:
   		autre bloc d'instructions à réaliser
   	...
   	```

	```
    # Avec if ... elif ... else
	if condition :
   		bloc d'instructions à réaliser
   	elif autre condition:
   		autre bloc d'instructions à réaliser
   	else:
        encore un autre bloc d'instructions à réaliser
   	...
   	```

## V. Exercices

???+ question "Exercice 1 **papier**"

	```python linenums='1'
	a = 10
	b = 13
	if a > 5:
	    b = b - 4
	if b >= 11:
	    b = b + 10
	```

    Que vaut la valeur finale de b ?

    === "Cocher la ou les affirmations correctes"

        - [ ] 13
        - [ ] 9
        - [ ] 19
        - [ ] 23

    === "Solution"

        - :x: ~~13~~ Faux car 10 > 5. La ligne 4 et donc exécutée.
        - :white_check_mark: 9 
        - :x: ~~19~~ 
        - :x: ~~23~~ Faux car à la ligne 4 on a une nouvelle affectation de b qui ne respecte plus la condition du if de la ligne 5.


???+ question "Exercice 2 'papier'"

	```python linenums='1'
	a = 5
	b = 14
	if a > 9:
	    b = b - 1
	if b >= 10:
	    b = b + 8
	```

    Que vaut la valeur finale de b ?

    === "Cocher la ou les affirmations correctes"

        - [ ] 22
        - [ ] 21
        - [ ] 13
        - [ ] 14

    === "Solution"

        - :white_check_mark: 22 
        - :x: ~~21~~ Faux car la ligne 4 n'est pas exécutée
        - :x: ~~13~~ Faux car la ligne 4 n'est pas exécutée
        - :x: ~~14~~ Faux car à la ligne 6 est exécutée


???+ question "Exercice 3 'papier'"

	```python linenums='1'
	a = 7
	b = 20
	if a > 5:
	    b = b - 3
	if b >= 15:
	    b = b + 7
	```

    Que vaut la valeur finale de b ?

    === "Cocher la ou les affirmations correctes"

        - [ ] 24
        - [ ] 17
        - [ ] 20
        - [ ] 27

    === "Solution"

        - :white_check_mark: 24 
        - :x: ~~17~~ Faux car la ligne 6 est aussi exécutée
        - :x: ~~20~~ Faux car la ligne 4 et la ligne 6 sont exécutées.
        - :x: ~~27~~ Faux car à la ligne 4 est exécutée


???+ question "Exercice 4 'papier'"

	```python linenums='1'
	a = 2
	b = 14
	if a >= 0:
	    b = b - 2
	else:
		b = b + 2	
	if b > 0:
	    a = a + 1
	else:
		a = a - 1
	```

    Que vaut la valeur finale de **`a`** ?

    === "Cocher la ou les affirmations correctes"

        - [ ] 3
        - [ ] 16
        - [ ] 1
        - [ ] 12

    === "Solution"

        - :white_check_mark: 3 
        - :x: ~~16~~ Faux car la ligne 6 n'est pas exécutée.
        - :x: ~~1~~ Faux car la ligne 10 n'est pas exécutée.
        - :x: ~~12~~ Faux car on cherche la valeur finale de a.

???+ question "Exercice 5 'papier'"

	```python linenums='1'
	a = -3
	b = -11
	if a >= 0:
	    b = b - 1
	else:
		b = b + 1	
	if b > 0:
	    a = a + 3
	else:
		a = a - 3
	```

    Que vaut la valeur finale de **`a`** ?

    === "Cocher la ou les affirmations correctes"

        - [ ] -10
        - [ ] -6
        - [ ] 0
        - [ ] -12

    === "Solution"

        - :x: ~~-10~~ La ligne 6 est exécutée, mais on demande a.
        - :white_check_mark: -6. La ligne 6 et la ligne 10 sont exécutée. On demande a.
        - :x: ~~0~~ Faux car la ligne 8 n'est pas exécutée.
        - :x: ~~-12~~ 

???+ question "Exercice 6 'papier'"

    ```python linenums='1'
    a = -7
    b = 14
    if a >= 0:
        b = b - 4
    else:
        b = b + 4   
    if b > 0:
        a = a + 1
    else:
        a = a - 1
    ```

    Que vaut la valeur finale de **`a`** ?

    === "Cocher la ou les affirmations correctes"

        - [ ] 10
        - [ ] -8
        - [ ] 18
        - [ ] -6

    === "Solution"

        - :x: ~~-10~~
        - :x: ~~-8~~. La ligne 10 n'est pas exécutée.
        - :x: ~~18~~ Faux car la ligne 6 est exécutée mais on demande a.
        - :white_check_mark: -6. La ligne 6 et la ligne 8 sont exécutées. On demande a.

???+ question "Exercice 7 'papier'"

    ```python linenums='1'
    a = 10
    b = -11
    if a >= 0:
        b = b - 3
    else:
        b = b + 3   
    if b > 0:
        a = a + 3
    else:
        a = a - 3
    ```

    Que vaut la valeur finale de **`a`** ?

    === "Cocher la ou les affirmations correctes"

        - [ ] -8
        - [ ] 7
        - [ ] -14
        - [ ] 13

    === "Solution"

        - :x: ~~-8~~ 
        - :white_check_mark: 7 . Les lignes 4 et 10 sont exécutées.
        - :x: ~~-14~~ Faux car la ligne 4 est exécutée mais on demande a.
        - :x: ~~13~~ Faux car la ligne 8 n'est pas exécutée. 


???+ question "Exercice 8 'papier'"

    ```python linenums='1'
    a = -20
    b = -7
    c = -2
    if a > b:
        d = a
    elif b > c:
        d = b
    else:
        d = c
    ```

    Que vaut la valeur finale de **`d`** ?

    ??? success "Solution"

        -2

???+ question "Exercice 9 'papier'"

    ```python linenums='1'
    a = 10
    b = 19
    c = -15
    if a > b:
        d = a
    elif b > c:
        d = b
    else:
        d = c
    ```

    Que vaut la valeur finale de **`d`** ?

    ??? success "Solution"

        19


