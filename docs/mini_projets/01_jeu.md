---
author: et Mireille Coilhac
title: Jeu de dé
---

## Votre travail

Ce mini-projet est à réaliser par deux, voter professeur vous indiquera avec qui vous devez travailler.  
Il faudra **pour chacun** déposer votre fichier `.py` sur classroom, avant mercredi 14/12/2022 à 23h59.
Le nom de votre fichier sera sur ce modèle (si votre nom est DUPOND) : `dupond.py` 

## Pénalités : 

* Moins un point par jour de retard.
* Moins dix points si le fichier est identique, ou quasiment identique à celui d’un autre groupe. 
De minimes différences sur les noms de variables, les nome de fonctions, ou la disposition du code ne sont pas considérées comme des différences


## Le sujet

Alice et Bob jouent au jeu suivant : un joueur lance 10 dés.  Si le joueur a obtenu au moins deux fois un « 6 », il marque 1 point.
S’il n’a obtenu aucun « 6 », il perd 1 point.
Chacun joue ainsi à tour de rôle.
C’est Alice qui commence à jouer.
La partie se termine **dès** qu'un joueur obtient 5 points. Ce joueur est alors le gagnant. L'autre joueur ne joue plus.
Ecrire un script Python qui simule ce jeu comme décrit plus bas. Vous utiliserez **obligatoirement** des 

* boucle while
* listes
* fonctions.
* Votre script devra contenir des commentaires.
* Il devra y avoir au moins un assert qui teste une fonction

Il est **interdit** d'utiliser l'instruction `break` (0 dans le barème pour le critère "Boucles correctes")

Exemple de ce que doit afficher votre programme : 

```pycon
Alice: [5, 2, 5, 1, 2, 6, 6, 5, 1, 6]
Le nombre de points d'Alice est: 1
Bob: [2, 5, 4, 5, 6, 5, 6, 6, 1, 1]
Le nombre de points de Bob est: 1
Alice: [5, 1, 5, 4, 3, 1, 4, 3, 3, 6]
Le nombre de points d'Alice est: 1
Bob: [3, 2, 2, 5, 3, 4, 2, 6, 3, 5]
Le nombre de points de Bob est: 1
Alice: [4, 6, 2, 1, 3, 6, 4, 2, 4, 6]
Le nombre de points d'Alice est: 2
Bob: [4, 2, 3, 1, 1, 1, 4, 2, 5, 5]
Le nombre de points de Bob est: 0
Alice: [6, 4, 1, 3, 3, 5, 4, 1, 6, 1]
Le nombre de points d'Alice est: 3
Bob: [2, 6, 5, 2, 2, 4, 3, 3, 2, 4]
Le nombre de points de Bob est: 0
Alice: [3, 6, 5, 6, 2, 3, 1, 3, 6, 3]
Le nombre de points d'Alice est: 4
Bob: [1, 3, 3, 4, 5, 2, 1, 1, 4, 3]
Le nombre de points de Bob est: -1
Alice: [4, 1, 6, 6, 4, 2, 2, 1, 3, 2]
Le nombre de points d'Alice est: 5
Alice a gagné
```

## Le barème : 

|Critère|sur|
|--:|--:|
|Respect des consignes de nom de fichier |2|
|Utilisation judicieuse des listes|2|
|Ecriture de fonctions correctes|2|
|Appel de fonctions corrects|2|
|Boucles correctes|2|
|assert correct|1|
|Choix des noms des variables et de fonctions|1,5|
|Commentaires|1,5|
|Syntaxe|2|
|Fonctionne comme demandé|4|

## 😊 Une correction possible : 


⏳ Attendre un peu pour la correction viendra bientôt ... 

<!--- La correction à télécharger plus tard 
{{IDE('scripts/cor_mini_1_2022')}}
-->


---

<center>

**Auteur** Mireille COILHAC - Lycée Saint-Aspais de Melun (77)

Publié sous licence CC BY-NC-SA


---
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" width="100" src="https://s2.qwant.com/thumbr/700x0/f/9/436dcce5e02185c0b2e1b0e7a969aa81d7b0438d0a7f2eaee89abb9661b70b/cc-by-nc-sa.svg_.png?u=http%3A%2F%2Fopensiddur.org%2Fwp-content%2Fuploads%2F2011%2F03%2Fcc-by-nc-sa.svg_.png&q=0&b=1&p=0&a=1" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>. 
